package edu.ntnu.idatt2003;

public class ViewApp {

    public static void main(String[] args) {
        View textView = new TextView();

        textView = new ScrollDecorator(textView);
        textView = new ResizeDecorator(textView);

        textView.draw();
    }
}
