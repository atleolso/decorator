package edu.ntnu.idatt2003;

public class ResizeDecorator extends ViewDecorator {

    public ResizeDecorator(View decoratedView) {
        super(decoratedView);
    }

    @Override
    public void draw() {
        decoratedView.draw();
        drawResizer();
    }

    private void drawResizer() {
        System.out.println("Draws resizer...");
    }
}
