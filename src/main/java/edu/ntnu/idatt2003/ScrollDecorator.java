package edu.ntnu.idatt2003;

public class ScrollDecorator extends ViewDecorator {

    public ScrollDecorator(View decoratedView) {
        super(decoratedView);
    }

    @Override
    public void draw() {
        decoratedView.draw();
        drawScrollbars();
    }

    private void drawScrollbars() {
        System.out.println("Draws scrollbars...");
    }
}
