package edu.ntnu.idatt2003;

public interface View {

    void draw();
}
