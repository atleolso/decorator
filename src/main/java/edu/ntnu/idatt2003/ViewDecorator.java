package edu.ntnu.idatt2003;

public abstract class ViewDecorator implements View {

    protected View decoratedView;

    public ViewDecorator(View decoratedView) {
        this.decoratedView = decoratedView;
    }

    @Override
    public void draw() {
        decoratedView.draw();
    }
}
